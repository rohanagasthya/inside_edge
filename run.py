#"""Main script for generating output.csv."""#
import pandas as pd

def main():
    df = pd.read_csv('./data/raw/pitchdata.csv') # reading in the file

    filt = (df['PitcherSide'] == 'R')
    df_RHP = df[filt] # filtering condition for right-handed pitchers
    df_RHP_grp = df_RHP.groupby(['HitterId']).sum() # grouping by HitterId
    filt = df_RHP_grp['PA'] >= 25
    df_RHP_grp_filt = df_RHP_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for RHP by HitterId
    AVG = (df_RHP_grp_filt['H']/df_RHP_grp_filt['AB'])
    df_RHP_avg = pd.concat([df_RHP_grp_filt, AVG], axis = 'columns')
    df_RHP_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_avg['Stat'] = 'AVG'
    df_RHP_avg['Split'] = 'vs RHP'
    df_RHP_avg['Subject'] = 'HitterId'
    df_RHP_avg = df_RHP_avg.reset_index()
    df_RHP_avg.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_RHP_avg_xyz = df_RHP_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for RHP by HitterId
    OBP = ((df_RHP_grp_filt['H'] + df_RHP_grp_filt['BB'] + df_RHP_grp_filt['HBP'])/(df_RHP_grp_filt['AB'] + df_RHP_grp_filt['BB'] + df_RHP_grp_filt['HBP'] + df_RHP_grp_filt['SF']))
    df_RHP_obp = pd.concat([df_RHP_grp_filt, OBP], axis = 'columns')
    df_RHP_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_obp['Stat'] = 'OBP'
    df_RHP_obp['Split'] = 'vs RHP'
    df_RHP_obp['Subject'] = 'HitterId'
    df_RHP_obp = df_RHP_obp.reset_index()
    df_RHP_obp.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_RHP_obp_xyz = df_RHP_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for RHP by HitterId
    SLG = (df_RHP_grp_filt['TB']/df_RHP_grp_filt['AB'])
    df_RHP_slg = pd.concat([df_RHP_grp_filt, SLG], axis = 'columns')
    df_RHP_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_slg['Stat'] = 'SLG'
    df_RHP_slg['Split'] = 'vs RHP'
    df_RHP_slg['Subject'] = 'HitterId'
    df_RHP_slg = df_RHP_slg.reset_index()
    df_RHP_slg.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_RHP_slg_xyz = df_RHP_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for RHP by HitterId
    OPS = (df_RHP_obp_xyz['Value'] + df_RHP_slg_xyz['Value'])
    df_RHP_ops_xyz = df_RHP_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_RHP_ops_xyz['Value'] = OPS
    df_RHP_ops_xyz['Stat'] = 'OPS'

    # concatenating all the RHP by HitterId dataframes into a single dataframe
    df_RHP_HI = pd.concat([df_RHP_avg_xyz, df_RHP_obp_xyz, df_RHP_slg_xyz, df_RHP_ops_xyz], ignore_index = True)

    filt = (df['PitcherSide'] == 'L')
    df_LHP = df[filt] # filtering condition for left-handed pitchers
    df_LHP_grp = df_LHP.groupby(['HitterId']).sum() # grouping by HitterId
    filt = df_LHP_grp['PA'] >= 25
    df_LHP_grp_filt = df_LHP_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for LHP by HitterId
    AVG = (df_LHP_grp_filt['H']/df_LHP_grp_filt['AB'])
    df_LHP_avg = pd.concat([df_LHP_grp_filt, AVG], axis = 'columns')
    df_LHP_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_avg['Stat'] = 'AVG'
    df_LHP_avg['Split'] = 'vs LHP'
    df_LHP_avg['Subject'] = 'HitterId'
    df_LHP_avg = df_LHP_avg.reset_index()
    df_LHP_avg.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_LHP_avg_xyz = df_LHP_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for LHP by HitterId
    OBP = ((df_LHP_grp_filt['H'] + df_LHP_grp_filt['BB'] + df_LHP_grp_filt['HBP'])/(df_LHP_grp_filt['AB'] + df_LHP_grp_filt['BB'] + df_LHP_grp_filt['HBP'] + df_LHP_grp_filt['SF']))
    df_LHP_obp = pd.concat([df_LHP_grp_filt, OBP], axis = 'columns')
    df_LHP_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_obp['Stat'] = 'OBP'
    df_LHP_obp['Split'] = 'vs LHP'
    df_LHP_obp['Subject'] = 'HitterId'
    df_LHP_obp = df_LHP_obp.reset_index()
    df_LHP_obp.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_LHP_obp_xyz = df_LHP_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for LHP by HitterId
    SLG = (df_LHP_grp_filt['TB']/df_LHP_grp_filt['AB'])
    df_LHP_slg = pd.concat([df_LHP_grp_filt, SLG], axis = 'columns')
    df_LHP_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_slg['Stat'] = 'SLG'
    df_LHP_slg['Split'] = 'vs LHP'
    df_LHP_slg['Subject'] = 'HitterId'
    df_LHP_slg = df_LHP_slg.reset_index()
    df_LHP_slg.rename(columns = {'HitterId' : 'SubjectId'}, inplace = True)
    df_LHP_slg_xyz = df_LHP_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for LHP by HitterId
    OPS = (df_LHP_obp_xyz['Value'] + df_LHP_slg_xyz['Value'])
    df_LHP_ops_xyz = df_LHP_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_LHP_ops_xyz['Value'] = OPS
    df_LHP_ops_xyz['Stat'] = 'OPS'

    # concatenating all the LHP by HitterId dataframes into a single dataframe
    df_LHP_HI = pd.concat([df_LHP_avg_xyz, df_LHP_obp_xyz, df_LHP_slg_xyz, df_LHP_ops_xyz], ignore_index = True)

    filt = (df['PitcherSide'] == 'R')
    df_RHP_HTI = df[filt] # filtering condition for right-handed pitchers
    df_RHP_HTI_grp = df_RHP_HTI.groupby(['HitterTeamId']).sum() # grouping by HitterTeamId
    filt = df_RHP_HTI_grp['PA'] >= 25
    df_RHP_HTI_grp_filt = df_RHP_HTI_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for RHP by HitterTeamId
    AVG = (df_RHP_HTI_grp_filt['H']/df_RHP_HTI_grp_filt['AB'])
    df_RHP_HTI_avg = pd.concat([df_RHP_HTI_grp_filt, AVG], axis = 'columns')
    df_RHP_HTI_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_HTI_avg['Stat'] = 'AVG'
    df_RHP_HTI_avg['Split'] = 'vs RHP'
    df_RHP_HTI_avg['Subject'] = 'HitterTeamId'
    df_RHP_HTI_avg = df_RHP_HTI_avg.reset_index()
    df_RHP_HTI_avg.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_RHP_HTI_avg_xyz = df_RHP_HTI_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for RHP by HitterTeamId
    OBP = ((df_RHP_HTI_grp_filt['H'] + df_RHP_HTI_grp_filt['BB'] + df_RHP_HTI_grp_filt['HBP'])/(df_RHP_HTI_grp_filt['AB'] + df_RHP_HTI_grp_filt['BB'] + df_RHP_HTI_grp_filt['HBP'] + df_RHP_HTI_grp_filt['SF']))
    df_RHP_HTI_obp = pd.concat([df_RHP_HTI_grp_filt, OBP], axis = 'columns')
    df_RHP_HTI_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_HTI_obp['Stat'] = 'OBP'
    df_RHP_HTI_obp['Split'] = 'vs RHP'
    df_RHP_HTI_obp['Subject'] = 'HitterTeamId'
    df_RHP_HTI_obp = df_RHP_HTI_obp.reset_index()
    df_RHP_HTI_obp.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_RHP_HTI_obp_xyz = df_RHP_HTI_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for RHP by HitterTeamId
    SLG = (df_RHP_HTI_grp_filt['TB']/df_RHP_HTI_grp_filt['AB'])
    df_RHP_HTI_slg = pd.concat([df_RHP_HTI_grp_filt, SLG], axis = 'columns')
    df_RHP_HTI_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHP_HTI_slg['Stat'] = 'SLG'
    df_RHP_HTI_slg['Split'] = 'vs RHP'
    df_RHP_HTI_slg['Subject'] = 'HitterTeamId'
    df_RHP_HTI_slg = df_RHP_HTI_slg.reset_index()
    df_RHP_HTI_slg.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_RHP_HTI_slg_xyz = df_RHP_HTI_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for RHP by HitterTeamId
    OPS = (df_RHP_HTI_obp_xyz['Value'] + df_RHP_HTI_slg_xyz['Value'])
    df_RHP_HTI_ops_xyz = df_RHP_HTI_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_RHP_HTI_ops_xyz['Value'] = OPS
    df_RHP_HTI_ops_xyz['Stat'] = 'OPS'

    # concatenating all the RHP by HitterTeamId dataframes into a single dataframe
    df_RHP_HTI = pd.concat([df_RHP_HTI_avg_xyz, df_RHP_HTI_obp_xyz, df_RHP_HTI_slg_xyz, df_RHP_HTI_ops_xyz], ignore_index = True)

    filt = (df['PitcherSide'] == 'L')
    df_LHP_HTI = df[filt] # filtering condition for left-handed pitchers
    df_LHP_HTI_grp = df_LHP_HTI.groupby(['HitterTeamId']).sum() # grouping by HitterTeamId
    filt = df_LHP_HTI_grp['PA'] >= 25
    df_LHP_HTI_grp_filt = df_LHP_HTI_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for LHP by HitterTeamId
    AVG = (df_LHP_HTI_grp_filt['H']/df_LHP_HTI_grp_filt['AB'])
    df_LHP_HTI_avg = pd.concat([df_LHP_HTI_grp_filt, AVG], axis = 'columns')
    df_LHP_HTI_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_HTI_avg['Stat'] = 'AVG'
    df_LHP_HTI_avg['Split'] = 'vs LHP'
    df_LHP_HTI_avg['Subject'] = 'HitterTeamId'
    df_LHP_HTI_avg = df_LHP_HTI_avg.reset_index()
    df_LHP_HTI_avg.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_LHP_HTI_avg_xyz = df_LHP_HTI_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for LHP by HitterTeamId
    OBP = ((df_LHP_HTI_grp_filt['H'] + df_LHP_HTI_grp_filt['BB'] + df_LHP_HTI_grp_filt['HBP'])/(df_LHP_HTI_grp_filt['AB'] + df_LHP_HTI_grp_filt['BB'] + df_LHP_HTI_grp_filt['HBP'] + df_LHP_HTI_grp_filt['SF']))
    df_LHP_HTI_obp = pd.concat([df_LHP_HTI_grp_filt, OBP], axis = 'columns')
    df_LHP_HTI_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_HTI_obp['Stat'] = 'OBP'
    df_LHP_HTI_obp['Split'] = 'vs LHP'
    df_LHP_HTI_obp['Subject'] = 'HitterTeamId'
    df_LHP_HTI_obp = df_LHP_HTI_obp.reset_index()
    df_LHP_HTI_obp.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_LHP_HTI_obp_xyz = df_LHP_HTI_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for LHP by HitterTeamId
    SLG = (df_LHP_HTI_grp_filt['TB']/df_LHP_HTI_grp_filt['AB'])
    df_LHP_HTI_slg = pd.concat([df_LHP_HTI_grp_filt, SLG], axis = 'columns')
    df_LHP_HTI_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHP_HTI_slg['Stat'] = 'SLG'
    df_LHP_HTI_slg['Split'] = 'vs LHP'
    df_LHP_HTI_slg['Subject'] = 'HitterTeamId'
    df_LHP_HTI_slg = df_LHP_HTI_slg.reset_index()
    df_LHP_HTI_slg.rename(columns = {'HitterTeamId' : 'SubjectId'}, inplace = True)
    df_LHP_HTI_slg_xyz = df_LHP_HTI_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for LHP by HitterTeamId
    OPS = (df_LHP_HTI_obp_xyz['Value'] + df_LHP_HTI_slg_xyz['Value'])
    df_LHP_HTI_ops_xyz = df_LHP_HTI_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_LHP_HTI_ops_xyz['Value'] = OPS
    df_LHP_HTI_ops_xyz['Stat'] = 'OPS'

    # concatenating all the LHP by HitterTeamId dataframes into a single dataframe
    df_LHP_HTI = pd.concat([df_LHP_HTI_avg_xyz, df_LHP_HTI_obp_xyz, df_LHP_HTI_slg_xyz, df_LHP_HTI_ops_xyz], ignore_index = True)

    filt = (df['HitterSide'] == 'R')
    df_RHH = df[filt] # filtering condition for right-handed hitters
    df_RHH_grp = df_RHH.groupby(['PitcherId']).sum() # grouping by PitcherId
    filt = df_RHH_grp['PA'] >= 25
    df_RHH_grp_filt = df_RHH_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for RHH by PitcherId
    AVG = (df_RHH_grp_filt['H']/df_RHH_grp_filt['AB'])
    df_RHH_avg = pd.concat([df_RHH_grp_filt, AVG], axis = 'columns')
    df_RHH_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_avg['Stat'] = 'AVG'
    df_RHH_avg['Split'] = 'vs RHH'
    df_RHH_avg['Subject'] = 'PitcherId'
    df_RHH_avg = df_RHH_avg.reset_index()
    df_RHH_avg.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_RHH_avg_xyz = df_RHH_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for RHH by PitcherId
    OBP = ((df_RHH_grp_filt['H'] + df_RHH_grp_filt['BB'] + df_RHH_grp_filt['HBP'])/(df_RHH_grp_filt['AB'] + df_RHH_grp_filt['BB'] + df_RHH_grp_filt['HBP'] + df_RHH_grp_filt['SF']))
    df_RHH_obp = pd.concat([df_RHH_grp_filt, OBP], axis = 'columns')
    df_RHH_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_obp['Stat'] = 'OBP'
    df_RHH_obp['Split'] = 'vs RHH'
    df_RHH_obp['Subject'] = 'PitcherId'
    df_RHH_obp = df_RHH_obp.reset_index()
    df_RHH_obp.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_RHH_obp_xyz = df_RHH_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for RHH by PitcherId
    SLG = (df_RHH_grp_filt['TB']/df_RHH_grp_filt['AB'])
    df_RHH_slg = pd.concat([df_RHH_grp_filt, SLG], axis = 'columns')
    df_RHH_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_slg['Stat'] = 'SLG'
    df_RHH_slg['Split'] = 'vs RHH'
    df_RHH_slg['Subject'] = 'PitcherId'
    df_RHH_slg = df_RHH_slg.reset_index()
    df_RHH_slg.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_RHH_slg_xyz = df_RHH_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for RHH by PitcherId
    OPS = (df_RHH_obp_xyz['Value'] + df_RHH_slg_xyz['Value'])
    df_RHH_ops_xyz = df_RHH_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_RHH_ops_xyz['Value'] = OPS
    df_RHH_ops_xyz['Stat'] = 'OPS'

    # concatenating all the RHH by PitcherId dataframes into a single dataframe
    df_RHH_PI = pd.concat([df_RHH_avg_xyz, df_RHH_obp_xyz, df_RHH_slg_xyz, df_RHH_ops_xyz], ignore_index = True)

    filt = (df['HitterSide'] == 'L')
    df_LHH = df[filt] # filtering condition for left-handed hitters
    df_LHH_grp = df_LHH.groupby(['PitcherId']).sum() # grouping by PitcherId
    filt = df_LHH_grp['PA'] >= 25
    df_LHH_grp_filt = df_LHH_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for LHH by PitcherId
    AVG = (df_LHH_grp_filt['H']/df_LHH_grp_filt['AB'])
    df_LHH_avg = pd.concat([df_LHH_grp_filt, AVG], axis = 'columns')
    df_LHH_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_avg['Stat'] = 'AVG'
    df_LHH_avg['Split'] = 'vs LHH'
    df_LHH_avg['Subject'] = 'PitcherId'
    df_LHH_avg = df_LHH_avg.reset_index()
    df_LHH_avg.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_LHH_avg_xyz = df_LHH_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for LHH by PitcherId
    OBP = ((df_LHH_grp_filt['H'] + df_LHH_grp_filt['BB'] + df_LHH_grp_filt['HBP'])/(df_LHH_grp_filt['AB'] + df_LHH_grp_filt['BB'] + df_LHH_grp_filt['HBP'] + df_LHH_grp_filt['SF']))
    df_LHH_obp = pd.concat([df_LHH_grp_filt, OBP], axis = 'columns')
    df_LHH_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_obp['Stat'] = 'OBP'
    df_LHH_obp['Split'] = 'vs LHH'
    df_LHH_obp['Subject'] = 'PitcherId'
    df_LHH_obp = df_LHH_obp.reset_index()
    df_LHH_obp.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_LHH_obp_xyz = df_LHH_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for LHH by PitcherId
    SLG = (df_LHH_grp_filt['TB']/df_LHH_grp_filt['AB'])
    df_LHH_slg = pd.concat([df_LHH_grp_filt, SLG], axis = 'columns')
    df_LHH_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_slg['Stat'] = 'SLG'
    df_LHH_slg['Split'] = 'vs LHH'
    df_LHH_slg['Subject'] = 'PitcherId'
    df_LHH_slg = df_LHH_slg.reset_index()
    df_LHH_slg.rename(columns = {'PitcherId' : 'SubjectId'}, inplace = True)
    df_LHH_slg_xyz = df_LHH_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for LHH by PitcherId
    OPS = (df_LHH_obp_xyz['Value'] + df_LHH_slg_xyz['Value'])
    df_LHH_ops_xyz = df_LHH_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_LHH_ops_xyz['Value'] = OPS
    df_LHH_ops_xyz['Stat'] = 'OPS'

    # concatenating all the LHH by PitcherId dataframes into a single dataframe
    df_LHH_PI = pd.concat([df_LHH_avg_xyz, df_LHH_obp_xyz, df_LHH_slg_xyz, df_LHH_ops_xyz], ignore_index = True)

    filt = (df['HitterSide'] == 'R')
    df_RHH_PTI = df[filt] # filtering condition for right-handed hitters
    df_RHH_PTI_grp = df_RHH_PTI.groupby(['PitcherTeamId']).sum() # grouping by PitcherTeamId
    filt = df_RHH_PTI_grp['PA'] >= 25
    df_RHH_PTI_grp_filt = df_RHH_PTI_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for RHH by PitcherTeamId
    AVG = (df_RHH_PTI_grp_filt['H']/df_RHH_PTI_grp_filt['AB'])
    df_RHH_PTI_avg = pd.concat([df_RHH_PTI_grp_filt, AVG], axis = 'columns')
    df_RHH_PTI_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_PTI_avg['Stat'] = 'AVG'
    df_RHH_PTI_avg['Split'] = 'vs RHH'
    df_RHH_PTI_avg['Subject'] = 'PitcherTeamId'
    df_RHH_PTI_avg = df_RHH_PTI_avg.reset_index()
    df_RHH_PTI_avg.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_RHH_PTI_avg_xyz = df_RHH_PTI_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for RHH by PitcherTeamId
    OBP = ((df_RHH_PTI_grp_filt['H'] + df_RHH_PTI_grp_filt['BB'] + df_RHH_PTI_grp_filt['HBP'])/(df_RHH_PTI_grp_filt['AB'] + df_RHH_PTI_grp_filt['BB'] + df_RHH_PTI_grp_filt['HBP'] + df_RHH_PTI_grp_filt['SF']))
    df_RHH_PTI_obp = pd.concat([df_RHH_PTI_grp_filt, OBP], axis = 'columns')
    df_RHH_PTI_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_PTI_obp['Stat'] = 'OBP'
    df_RHH_PTI_obp['Split'] = 'vs RHH'
    df_RHH_PTI_obp['Subject'] = 'PitcherTeamId'
    df_RHH_PTI_obp = df_RHH_PTI_obp.reset_index()
    df_RHH_PTI_obp.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_RHH_PTI_obp_xyz = df_RHH_PTI_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for RHH by PitcherTeamId
    SLG = (df_RHH_PTI_grp_filt['TB']/df_RHH_PTI_grp_filt['AB'])
    df_RHH_PTI_slg = pd.concat([df_RHH_PTI_grp_filt, SLG], axis = 'columns')
    df_RHH_PTI_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_RHH_PTI_slg['Stat'] = 'SLG'
    df_RHH_PTI_slg['Split'] = 'vs RHH'
    df_RHH_PTI_slg['Subject'] = 'PitcherTeamId'
    df_RHH_PTI_slg = df_RHH_PTI_slg.reset_index()
    df_RHH_PTI_slg.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_RHH_PTI_slg_xyz = df_RHH_PTI_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for RHH by PitcherTeamId
    OPS = (df_RHH_PTI_obp_xyz['Value'] + df_RHH_PTI_slg_xyz['Value'])
    df_RHH_PTI_ops_xyz = df_RHH_PTI_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_RHH_PTI_ops_xyz['Value'] = OPS
    df_RHH_PTI_ops_xyz['Stat'] = 'OPS'

    # concatenating all the RHH by PitcherTeamId dataframes into a single dataframe
    df_RHH_PTI = pd.concat([df_RHH_PTI_avg_xyz, df_RHH_PTI_obp_xyz, df_RHH_PTI_slg_xyz, df_RHH_PTI_ops_xyz], ignore_index = True)

    filt = (df['HitterSide'] == 'L')
    df_LHH_PTI = df[filt] # filtering condition for left-handed hitters
    df_LHH_PTI_grp = df_LHH_PTI.groupby(['PitcherTeamId']).sum() # grouping by PitcherTeamId
    filt = df_LHH_PTI_grp['PA'] >= 25
    df_LHH_PTI_grp_filt = df_LHH_PTI_grp.loc[filt] # filtering only for records with PA >= 25

    # AVG for LHH by PitcherTeamId
    AVG = (df_LHH_PTI_grp_filt['H']/df_LHH_PTI_grp_filt['AB'])
    df_LHH_PTI_avg = pd.concat([df_LHH_PTI_grp_filt, AVG], axis = 'columns')
    df_LHH_PTI_avg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_PTI_avg['Stat'] = 'AVG'
    df_LHH_PTI_avg['Split'] = 'vs LHH'
    df_LHH_PTI_avg['Subject'] = 'PitcherTeamId'
    df_LHH_PTI_avg = df_LHH_PTI_avg.reset_index()
    df_LHH_PTI_avg.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_LHH_PTI_avg_xyz = df_LHH_PTI_avg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OBP for LHH by PitcherTeamId
    OBP = ((df_LHH_PTI_grp_filt['H'] + df_LHH_PTI_grp_filt['BB'] + df_LHH_PTI_grp_filt['HBP'])/(df_LHH_PTI_grp_filt['AB'] + df_LHH_PTI_grp_filt['BB'] + df_LHH_PTI_grp_filt['HBP'] + df_LHH_PTI_grp_filt['SF']))
    df_LHH_PTI_obp = pd.concat([df_LHH_PTI_grp_filt, OBP], axis = 'columns')
    df_LHH_PTI_obp.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_PTI_obp['Stat'] = 'OBP'
    df_LHH_PTI_obp['Split'] = 'vs LHH'
    df_LHH_PTI_obp['Subject'] = 'PitcherTeamId'
    df_LHH_PTI_obp = df_LHH_PTI_obp.reset_index()
    df_LHH_PTI_obp.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_LHH_PTI_obp_xyz = df_LHH_PTI_obp.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # SLG for LHH by PitcherTeamId
    SLG = (df_LHH_PTI_grp_filt['TB']/df_LHH_PTI_grp_filt['AB'])
    df_LHH_PTI_slg = pd.concat([df_LHH_PTI_grp_filt, SLG], axis = 'columns')
    df_LHH_PTI_slg.rename(columns = {0 : 'Value'}, inplace = True)
    df_LHH_PTI_slg['Stat'] = 'SLG'
    df_LHH_PTI_slg['Split'] = 'vs LHH'
    df_LHH_PTI_slg['Subject'] = 'PitcherTeamId'
    df_LHH_PTI_slg = df_LHH_PTI_slg.reset_index()
    df_LHH_PTI_slg.rename(columns = {'PitcherTeamId' : 'SubjectId'}, inplace = True)
    df_LHH_PTI_slg_xyz = df_LHH_PTI_slg.loc[:, ['SubjectId', 'Stat', 'Split', 'Subject', 'Value']]

    # OPS for LHH by PitcherTeamId
    OPS = (df_LHH_PTI_obp_xyz['Value'] + df_LHH_PTI_slg_xyz['Value'])
    df_LHH_PTI_ops_xyz = df_LHH_PTI_slg_xyz.loc[:, 'SubjectId' : 'Subject']
    df_LHH_PTI_ops_xyz['Value'] = OPS
    df_LHH_PTI_ops_xyz['Stat'] = 'OPS'

    # concatenating all the LHH by PitcherTeamId dataframes into a single dataframe
    df_LHH_PTI = pd.concat([df_LHH_PTI_avg_xyz, df_LHH_PTI_obp_xyz, df_LHH_PTI_slg_xyz, df_LHH_PTI_ops_xyz], ignore_index = True)

    # concatenating all the above concatenated dataframes into a single final dataframe
    final_df = pd.concat([df_RHP_HI, df_LHP_HI, df_RHP_HTI, df_LHP_HTI, df_RHH_PI, df_LHH_PI, df_RHH_PTI, df_LHH_PTI], ignore_index = True)

    # rounding off the stat values to 3 decimal places
    final_df = final_df.round(3)

    # sorting the dataframe on the first four columns in ascending order
    final_df.sort_values(by = ['SubjectId', 'Stat', 'Split', 'Subject'], axis = 'index', ignore_index = True, inplace = True)
    final_df = final_df.set_index('SubjectId')
    # converting the dataframe into a csv file
    final_df.to_csv('./data/processed/output.csv')

    pass


if __name__ == '__main__':
    main()
